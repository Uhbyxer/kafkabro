package yo.kafka.bro.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/topics/{topicName}/messages")
@Slf4j
@RequiredArgsConstructor
public class MessageController {

    private final KafkaTemplate<String, String> kafkaTemplate;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void postMessage(@PathVariable String topicName, @RequestBody String message) {
        log.info("Sending message to topic: {}. Message: {}", topicName, message);
        kafkaTemplate.send(topicName, message);
    }
}
