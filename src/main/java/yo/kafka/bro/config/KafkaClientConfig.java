package yo.kafka.bro.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.Admin;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.Map;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Configuration
@Slf4j
public class KafkaClientConfig {

    private final Map<String, Object> preparedConfig;

    public KafkaClientConfig(KafkaConfigProps kafkaConfigProps) {
        preparedConfig = prepareNonBlankKafkaConfigProps(kafkaConfigProps);
        log.info("Kafka configuration: {}", preparedConfig);
    }

    @Bean
    public Admin adminClient() {
        return Admin.create(preparedConfig);
    }

    @Bean
    public KafkaTemplate<String, String> kafkaTemplate() {
        StringSerializer stringSerializer = new StringSerializer();
        DefaultKafkaProducerFactory<String, String> producerFactory =
                new DefaultKafkaProducerFactory<>(preparedConfig, stringSerializer, stringSerializer);

        return new KafkaTemplate<>(producerFactory);
    }

    private Map<String, Object> prepareNonBlankKafkaConfigProps(KafkaConfigProps kafkaConfigProps) {
        return kafkaConfigProps
                .getConfig()
                .entrySet()
                .stream()
                .filter(prop -> nonNull(prop.getValue()) && isNotBlank(prop.getValue().toString()))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
