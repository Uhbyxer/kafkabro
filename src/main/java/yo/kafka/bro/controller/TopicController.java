package yo.kafka.bro.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import yo.kafka.bro.service.TopicService;

import java.util.Set;

@RestController
@RequestMapping("/topics")
@Slf4j
@RequiredArgsConstructor
public class TopicController {
    private final TopicService topicService;

    @GetMapping
    public Set<String> getTopicNames() {
        Set<String> topicNames = topicService.getAllTopicNames();
        log.info("Get topic names: {}", topicNames);
        return topicNames;
    }
}
