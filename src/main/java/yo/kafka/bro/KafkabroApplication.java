package yo.kafka.bro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkabroApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkabroApplication.class, args);
	}

}
