package yo.kafka.bro.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.Admin;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class TopicService {
    private final Admin adminClient;

    public Set<String> getAllTopicNames() {
        log.info("Try to get topic names");

        try {
            return adminClient.listTopics().names().get();
        } catch (Exception e) {
            log.error("Can not get list of topics", e);
            throw new RuntimeException(e);
        }
    }
}
